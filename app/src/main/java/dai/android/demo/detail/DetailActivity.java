package dai.android.demo.detail;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import dai.android.app.R;
import dai.android.play.PlayerManager;
import dai.android.play.VideoView;

public class DetailActivity extends Activity {

    private PlayerManager mPlayerManager;

    private VideoView mVideoView;
    private Button mBtnPlayerOperator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        //setContentView(R.layout.activity_detail1);

        mVideoView = findViewById(R.id.video);

        mBtnPlayerOperator = findViewById(R.id.btnPlayOperator);
        mBtnPlayerOperator.setOnClickListener(mOnClickListener);

        findViewById(R.id.btnFullScreen).setOnClickListener(mOnClickListener);
        //findViewById(R.id.btnPause).setOnClickListener(mOnClickListener);

        mPlayerManager = new PlayerManager(mVideoView);
    }

    @Override
    public void onBackPressed() {

        if (null != mPlayerManager) {
            mPlayerManager.setFullScreen(false);
        }

        super.onBackPressed();
    }

    //private static final String URL = "http://tanzi27niu.cdsb.mobi/wps/wp-content/uploads/2017/05/2017-05-17_17-33-30.mp4";
    //private static final String URL = "http://10.200.11.202:8089/download/Videos/Demi-Lovato-Let_It_Go.mp4";
    private static final String URL = "http://10.200.11.202:8089/download/4k/4K_2160p-MengLiShuiXiang.mp4";
    //private static final String URL = "http://10.200.11.202:8089/download/4k/Girls.Generation.Oh.4in1-DD%2B3840x2160.mp4";
    //private static final String URL = "http://10.200.11.202:8089/download/Videos/4K_HuDie_30fps_AVC1_AAC.mp4";
    //private static final String URL = "http://10.200.11.202:8089/download/Videos/HD.Club-4K-Chimei-inn-3840x2160_29.97fps_AVC_AAC.mp4";
    //private static final String URL = "http://10.200.11.202:8089/download/Videos/Canal%2B4k.Demo.Trailer.2160p.HDTV.H.264.MP3.2.0-jTV.mkv";
    //private static final String URL = "http://ivi.bupt.edu.cn/hls/cctv5phd.m3u8";
    //private static final String URL = "http://ivi.bupt.edu.cn/hls/cctv5.m3u8";

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnPlayOperator: {
                    //mPlayerManager.setFullScreen(true);
                    if (mVideoView.isIdle() || mVideoView.isCompleted() || mVideoView.isError()) {
                        mVideoView.setPlayer(null);
                        mVideoView.setUp(URL, null);
                        mVideoView.start();

                        mBtnPlayerOperator.setText(R.string.strPause);

                    } else if (mVideoView.isPaused()) {
                        mVideoView.restart();

                        mBtnPlayerOperator.setText(R.string.strPause);

                    } else if (mVideoView.isPlaying()) {
                        mVideoView.pause();

                        mBtnPlayerOperator.setText(R.string.strPlay);
                    }

                    //mVideoView.setFullScreen(true);
                    break;
                }

                case R.id.btnFullScreen: {
                    mVideoView.setFullScreen(true);
                }

                //case R.id.btnPause: {
                //    if (mVideoView.isPlaying()) {
                //        mVideoView.pause();
                //    }
                //}
            }
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (null != mPlayerManager && mPlayerManager.isFullScreen()) {
                mPlayerManager.setFullScreen(false);
            } else {
                exit();
            }
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mVideoView != null) {
            mVideoView.release();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mVideoView != null) {
            mVideoView.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mVideoView != null) {
            mVideoView.restart();
        }
    }

    // press twice exit this application
    private long mExitStartTime = 0L;

    private void exit() {
        if ((System.currentTimeMillis() - mExitStartTime) > 2000) {
            Toast.makeText(getApplicationContext(), getString(R.string.strTwiceExit), Toast.LENGTH_LONG).show();
            mExitStartTime = System.currentTimeMillis();
        } else {
            onBackPressed();
        }
    }
}
