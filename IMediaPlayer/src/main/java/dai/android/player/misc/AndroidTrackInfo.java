package dai.android.player.misc;

import android.annotation.TargetApi;
import android.media.MediaFormat;
import android.media.MediaPlayer;
import android.os.Build;

public class AndroidTrackInfo implements ITrackInfo {

    public static AndroidTrackInfo[] fromMediaPlayer(MediaPlayer player) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            return fromTrackInfo(player.getTrackInfo());

        return null;
    }

    private static AndroidTrackInfo[] fromTrackInfo(MediaPlayer.TrackInfo[] trackInfos) {
        if (null == trackInfos) return null;

        AndroidTrackInfo[] infos = new AndroidTrackInfo[trackInfos.length];
        for (int i = 0; i < trackInfos.length; ++i) {
            infos[i] = new AndroidTrackInfo(trackInfos[i]);
        }
        return infos;
    }


    private final MediaPlayer.TrackInfo mTrackInfo;

    private AndroidTrackInfo(MediaPlayer.TrackInfo trackInfo) {
        mTrackInfo = trackInfo;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public IMediaFormat getFormat() {
        if (mTrackInfo == null)
            return null;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT)
            return null;

        MediaFormat mediaFormat = mTrackInfo.getFormat();
        if (mediaFormat == null)
            return null;

        return new AndroidMediaFormat(mediaFormat);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public String getLanguage() {
        if (mTrackInfo == null)
            return "und";

        return mTrackInfo.getLanguage();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public int getTrackType() {
        if (mTrackInfo == null)
            return MEDIA_TRACK_TYPE_UNKNOWN;

        return mTrackInfo.getTrackType();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public String getInfoInline() {
        if (mTrackInfo != null) {
            return mTrackInfo.toString();
        } else {
            return "null";
        }
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder(128);
        out.append(getClass().getSimpleName());
        out.append('{');
        if (mTrackInfo != null) {
            out.append(mTrackInfo.toString());
        } else {
            out.append("null");
        }
        out.append('}');
        return out.toString();
    }
}
