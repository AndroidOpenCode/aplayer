package dai.android.player;

import android.graphics.SurfaceTexture;

public interface ISurfaceTextureHost {
    void releaseSurfaceTexture(SurfaceTexture surfaceTexture);
}
