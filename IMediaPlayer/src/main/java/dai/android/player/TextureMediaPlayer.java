package dai.android.player;

import android.annotation.TargetApi;
import android.graphics.SurfaceTexture;
import android.os.Build;
import android.view.Surface;
import android.view.SurfaceHolder;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class TextureMediaPlayer extends MediaPlayerProxy
        implements IMediaPlayer, ISurfaceTextureHolder {

    private SurfaceTexture mSurfaceTexture;
    private ISurfaceTextureHost mSurfaceTextureHost;

    public TextureMediaPlayer(IMediaPlayer backEndMediaPlayer) {
        super(backEndMediaPlayer);
    }

    public void releaseSurfaceTexture() {
        if (null != mSurfaceTexture) {
            if (null != mSurfaceTextureHost) {
                mSurfaceTextureHost.releaseSurfaceTexture(mSurfaceTexture);
            } else {
                mSurfaceTexture.release();
            }

            mSurfaceTexture = null;
        }
    }

    //----------------------------------------------------------------------------------------------
    // override IMediaPlayer api
    //----------------------------------------------------------------------------------------------

    @Override
    public void reset() {
        super.reset();
        releaseSurfaceTexture();
    }

    @Override
    public void release() {
        super.release();
        releaseSurfaceTexture();
    }

    @Override
    public void setDisplay(SurfaceHolder surfaceHolder) {
        if (mSurfaceTexture == null) {
            super.setDisplay(surfaceHolder);
        }
    }

    @Override
    public void setSurface(Surface surface) {
        if (mSurfaceTexture == null) {
            super.setSurface(surface);
        }
    }

    //----------------------------------------------------------------------------------------------
    // override ISurfaceTextureHolder api
    //----------------------------------------------------------------------------------------------

    @Override
    public void setSurfaceTexture(SurfaceTexture surfaceTexture) {
        if (mSurfaceTexture == surfaceTexture)
            return;

        releaseSurfaceTexture();
        mSurfaceTexture = surfaceTexture;
        if (null == surfaceTexture) {
            super.setSurface(null);
        } else {
            super.setSurface(new Surface(surfaceTexture));
        }
    }

    @Override
    public SurfaceTexture getSurfaceTexture() {
        return mSurfaceTexture;
    }

    @Override
    public void setSurfaceTextureHost(ISurfaceTextureHost surfaceTextureHost) {
        mSurfaceTextureHost = surfaceTextureHost;
    }
}
