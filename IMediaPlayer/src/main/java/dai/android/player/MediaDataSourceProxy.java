package dai.android.player;

import android.annotation.TargetApi;
import android.media.MediaDataSource;
import android.os.Build;

import java.io.IOException;

import dai.android.player.misc.IMediaDataSource;

@TargetApi(Build.VERSION_CODES.M)
class MediaDataSourceProxy extends MediaDataSource {

    private final IMediaDataSource mMediaDataSource;

    public MediaDataSourceProxy(IMediaDataSource dataSource) {
        mMediaDataSource = dataSource;
    }

    @Override
    public int readAt(long position, byte[] buffer, int offset, int size)
            throws IOException {
        if (null != mMediaDataSource) {
            mMediaDataSource.readAt(position, buffer, offset, size);
        }
        return 0;
    }

    @Override
    public long getSize() throws IOException {
        if (null != mMediaDataSource) {
            return mMediaDataSource.getSize();
        }
        return 0;
    }

    @Override
    public void close() throws IOException {
        if (null != mMediaDataSource) {
            mMediaDataSource.close();
        }
    }
}
