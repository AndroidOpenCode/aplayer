package dai.android.player;

import android.graphics.Rect;

public final class dTimedText {

    private Rect mTextBounds = null;
    private String mTextChars = null;

    public dTimedText(Rect bounds, String text) {
        mTextBounds = bounds;
        mTextChars = text;
    }

    public Rect getBounds() {
        return mTextBounds;
    }

    public String getText() {
        return mTextChars;
    }

}
