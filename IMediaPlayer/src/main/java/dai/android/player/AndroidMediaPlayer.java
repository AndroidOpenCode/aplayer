package dai.android.player;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaDataSource;
import android.media.MediaPlayer;
import android.media.TimedText;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.view.Surface;
import android.view.SurfaceHolder;

import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Map;

import dai.android.player.misc.AndroidTrackInfo;
import dai.android.player.misc.IMediaDataSource;
import dai.android.player.misc.ITrackInfo;

public class AndroidMediaPlayer extends AbstractMediaPlayer {

    private static MediaInfo sMediaInfo;

    private final MediaPlayer mInternalMediaPlayer;
    private final AndroidMediaPlayerListenerHolder mInternalListenerAdapter;

    private final Object mInitLock = new Object();

    private MediaDataSource mMediaDataSource;
    private String mDataSource;
    private boolean mIsReleased;

    public AndroidMediaPlayer() {
        synchronized (mInitLock) {
            mInternalMediaPlayer = new MediaPlayer();
        }
        mInternalMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mInternalListenerAdapter = new AndroidMediaPlayerListenerHolder(this);

        attachInternalListeners();
    }


    @Override
    public void setDisplay(SurfaceHolder surfaceHolder) {
        synchronized (mInitLock) {
            if (!mIsReleased) {
                mInternalMediaPlayer.setDisplay(surfaceHolder);
            }
        }
    }

    @Override
    public void setDataSource(Context context, Uri uri)
            throws IOException,
            IllegalArgumentException,
            SecurityException,
            IllegalStateException {
        mInternalMediaPlayer.setDataSource(context, uri);
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void setDataSource(Context context, Uri uri, Map<String, String> headers)
            throws IOException,
            IllegalArgumentException,
            SecurityException,
            IllegalStateException {
        mInternalMediaPlayer.setDataSource(context, uri, headers);
    }

    @Override
    public void setDataSource(FileDescriptor fd)
            throws IOException, IllegalArgumentException, IllegalStateException {
        mInternalMediaPlayer.setDataSource(fd);
    }

    @Override
    public void resetListeners() {
        super.resetListeners();
    }

    @Override
    public void setDataSource(String path)
            throws IOException,
            IllegalArgumentException,
            SecurityException,
            IllegalStateException {
        mDataSource = path;
        Uri uri = Uri.parse(path);
        String scheme = uri.getScheme();
        if (!TextUtils.isEmpty(scheme) && scheme.equalsIgnoreCase("file")) {
            mInternalMediaPlayer.setDataSource(uri.getPath());
        } else {
            mInternalMediaPlayer.setDataSource(path);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void setDataSource(IMediaDataSource mediaDataSource) {
        releaseMediaDataSource();

        mMediaDataSource = new MediaDataSourceProxy(mediaDataSource);
        mInternalMediaPlayer.setDataSource(mMediaDataSource);
    }

    @Override
    public String getDataSource() {
        return mDataSource;
    }

    @Override
    public void prepareAsync() throws IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.prepareAsync();
        }
    }

    @Override
    public void start() throws IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.start();
        }
    }

    @Override
    public void stop() throws IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.stop();
        }
    }

    @Override
    public void pause() throws IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.pause();
        }
    }

    @Override
    public void setScreenOnWhilePlaying(boolean screenOn) {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setScreenOnWhilePlaying(screenOn);
        }
    }

    @Override
    public int getVideoWidth() {
        if (null != mInternalMediaPlayer) {
            return mInternalMediaPlayer.getVideoWidth();
        }
        return 0;
    }

    @Override
    public int getVideoHeight() {
        if (null != mInternalMediaPlayer) {
            return mInternalMediaPlayer.getVideoHeight();
        }
        return 0;
    }

    @Override
    public boolean isPlaying() {
        if (null != mInternalMediaPlayer) {
            try {
                return mInternalMediaPlayer.isPlaying();
            } catch (IllegalStateException e) {
                return false;
            }
        }

        return false;
    }

    @Override
    public void seekTo(long msec) throws IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.seekTo((int) msec);
        }
    }

    @Override
    public long getCurrentPosition() {
        try {
            return mInternalMediaPlayer.getCurrentPosition();
        } catch (IllegalStateException e) {
            return 0;
        }
    }

    @Override
    public long getDuration() {
        try {
            return mInternalMediaPlayer.getDuration();
        } catch (IllegalStateException e) {
            return 0;
        }
    }

    @Override
    public void release() {
        mIsReleased = true;
        mInternalMediaPlayer.release();
        releaseMediaDataSource();
        resetListeners();
        attachInternalListeners();
    }

    @Override
    public void reset() {
        try {
            mInternalMediaPlayer.reset();
        } catch (IllegalStateException e) {
            //DebugLog.printStackTrace(e);
        }
        releaseMediaDataSource();
        resetListeners();
        attachInternalListeners();
    }

    @Override
    public void setVolume(float leftVolume, float rightVolume) {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setVolume(leftVolume, rightVolume);
        }
    }

    @Override
    public int getAudioSessionId() {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.getAudioSessionId();
        }
        return 0;
    }

    @Override
    public MediaInfo getMediaInfo() {
        if (null == sMediaInfo) {
            MediaInfo info = new MediaInfo();
            info.videoDecoder = "android";
            info.videoDecoderImpl = "HW";

            info.audioDecoder = "android";
            info.audioDecoderImpl = "HW";

            sMediaInfo = info;
        }
        return sMediaInfo;
    }

    @Deprecated
    @Override
    public void setLogEnabled(boolean enable) {
    }

    @Deprecated
    @Override
    public boolean isPlayable() {
        return true;
    }

    @Override
    public void setAudioStreamType(int streamType) {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setAudioStreamType(streamType);
        }
    }

    @Deprecated
    @Override
    public void setKeepInBackground(boolean keepInBackground) {
    }

    @Override
    public int getVideoSarNum() {
        return 1;
    }

    @Override
    public int getVideoSarDen() {
        return 1;
    }

    @Deprecated
    @Override
    public void setWakeMode(Context context, int mode) {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setWakeMode(context, mode);
        }
    }

    @Override
    public void setLooping(boolean looping) {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setLooping(looping);
        }
    }

    @Override
    public boolean isLooping() {
        return null != mInternalMediaPlayer && mInternalMediaPlayer.isLooping();
    }

    @Override
    public ITrackInfo[] getTrackInfo() {
        if (null != mInternalMediaPlayer) {
            return AndroidTrackInfo.fromMediaPlayer(mInternalMediaPlayer);
        }
        return null;
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void setSurface(Surface surface) {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setSurface(surface);
        }
    }


    private void releaseMediaDataSource() {
        if (null != mMediaDataSource) {
            try {
                mMediaDataSource.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mMediaDataSource = null;
        }
    }


    private void attachInternalListeners() {
        mInternalMediaPlayer.setOnPreparedListener(mInternalListenerAdapter);
        mInternalMediaPlayer.setOnBufferingUpdateListener(mInternalListenerAdapter);
        mInternalMediaPlayer.setOnCompletionListener(mInternalListenerAdapter);
        mInternalMediaPlayer.setOnSeekCompleteListener(mInternalListenerAdapter);
        mInternalMediaPlayer.setOnVideoSizeChangedListener(mInternalListenerAdapter);
        mInternalMediaPlayer.setOnErrorListener(mInternalListenerAdapter);
        mInternalMediaPlayer.setOnInfoListener(mInternalListenerAdapter);
        mInternalMediaPlayer.setOnTimedTextListener(mInternalListenerAdapter);
    }


    /*----------------------------------------------------------------------------------------------
     * class AndroidMediaPlayerListenerHolder
     ---------------------------------------------------------------------------------------------*/
    private class AndroidMediaPlayerListenerHolder implements
            MediaPlayer.OnPreparedListener,
            MediaPlayer.OnCompletionListener,
            MediaPlayer.OnBufferingUpdateListener,
            MediaPlayer.OnSeekCompleteListener,
            MediaPlayer.OnVideoSizeChangedListener,
            MediaPlayer.OnErrorListener,
            MediaPlayer.OnInfoListener,
            MediaPlayer.OnTimedTextListener {

        public final WeakReference<AndroidMediaPlayer> mWeakMediaPlayer;

        public AndroidMediaPlayerListenerHolder(AndroidMediaPlayer mp) {
            mWeakMediaPlayer = new WeakReference<>(mp);
        }

        @Override
        public void onBufferingUpdate(MediaPlayer mp, int percent) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            if (self == null)
                return;
            notifyOnBufferingUpdate(percent);
        }

        @Override
        public void onCompletion(MediaPlayer mp) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            if (self == null)
                return;
            notifyOnCompletion();
        }

        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            return null != self && notifyOnError(what, extra);
        }

        @Override
        public boolean onInfo(MediaPlayer mp, int what, int extra) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            return null != self && notifyOnInfo(what, extra);
        }

        @Override
        public void onPrepared(MediaPlayer mp) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            if (self == null)
                return;
            notifyOnPrepared();
        }

        @Override
        public void onSeekComplete(MediaPlayer mp) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            if (null == self)
                return;
            notifyOnCompletion();
        }

        @Override
        public void onTimedText(MediaPlayer mp, TimedText text) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            if (null == self) return;

            dTimedText ijkText = null;
            if (text != null) {
                ijkText = new dTimedText(text.getBounds(), text.getText());
            }
            notifyOnTimedText(ijkText);
        }

        @Override
        public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            if (null == self) return;

            notifyOnVideoSizeChanged(width, height, 1, 1);
        }
    }
}
