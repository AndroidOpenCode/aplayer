package dai.android.player;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.view.Surface;
import android.view.SurfaceHolder;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.Map;

import dai.android.player.misc.IMediaDataSource;
import dai.android.player.misc.ITrackInfo;

public class MediaPlayerProxy implements IMediaPlayer {

    protected final IMediaPlayer mBackEndMediaPlayer;

    public MediaPlayerProxy(IMediaPlayer backEndMediaPlayer) {
        mBackEndMediaPlayer = backEndMediaPlayer;
    }

    public IMediaPlayer getInternalMediaPlayer() {
        return mBackEndMediaPlayer;
    }

    @Override
    public void setDisplay(SurfaceHolder surfaceHolder) {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.setDisplay(surfaceHolder);
        }
    }

    @Override
    public void setDataSource(Context context, Uri uri)
            throws IOException,
            IllegalArgumentException,
            SecurityException,
            IllegalStateException {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.setDataSource(context, uri);
        }
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void setDataSource(Context context, Uri uri, Map<String, String> headers)
            throws IOException,
            IllegalArgumentException,
            SecurityException,
            IllegalStateException {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.setDataSource(context, uri, headers);
        }
    }

    @Override
    public void setDataSource(FileDescriptor fd)
            throws IOException,
            IllegalArgumentException,
            IllegalStateException {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.setDataSource(fd);
        }
    }

    @Override
    public void setDataSource(String path)
            throws IOException,
            IllegalArgumentException,
            SecurityException,
            IllegalStateException {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.setDataSource(path);
        }
    }

    @Override
    public String getDataSource() {
        return null != mBackEndMediaPlayer ? mBackEndMediaPlayer.getDataSource() : null;
    }

    @Override
    public void prepareAsync() throws IllegalStateException {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.prepareAsync();
        }
    }

    @Override
    public void start() throws IllegalStateException {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.start();
        }
    }

    @Override
    public void stop() throws IllegalStateException {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.stop();
        }
    }

    @Override
    public void pause() throws IllegalStateException {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.pause();
        }
    }

    @Override
    public void setScreenOnWhilePlaying(boolean screenOn) {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.setScreenOnWhilePlaying(screenOn);
        }
    }

    @Override
    public int getVideoWidth() {
        return null != mBackEndMediaPlayer ? mBackEndMediaPlayer.getVideoWidth() : 0;
    }

    @Override
    public int getVideoHeight() {
        return null != mBackEndMediaPlayer ? mBackEndMediaPlayer.getVideoHeight() : 0;
    }

    @Override
    public boolean isPlaying() {
        return null != mBackEndMediaPlayer && mBackEndMediaPlayer.isPlaying();
    }

    @Override
    public void seekTo(long msec) throws IllegalStateException {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.seekTo(msec);
        }
    }

    @Override
    public long getCurrentPosition() {
        return null != mBackEndMediaPlayer ? mBackEndMediaPlayer.getCurrentPosition() : 0;
    }

    @Override
    public long getDuration() {
        return null != mBackEndMediaPlayer ? mBackEndMediaPlayer.getDuration() : 0;
    }

    @Override
    public void release() {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.release();
        }
    }

    @Override
    public void reset() {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.reset();
        }
    }

    @Override
    public void setVolume(float leftVolume, float rightVolume) {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.setVolume(leftVolume, rightVolume);
        }
    }

    @Override
    public int getAudioSessionId() {
        return null != mBackEndMediaPlayer ? mBackEndMediaPlayer.getAudioSessionId() : 0;
    }

    @Override
    public MediaInfo getMediaInfo() {
        return null != mBackEndMediaPlayer ? mBackEndMediaPlayer.getMediaInfo() : null;
    }

    @SuppressWarnings("EmptyMethod")
    @Deprecated
    @Override
    public void setLogEnabled(boolean enable) {
    }

    @Deprecated
    @Override
    public boolean isPlayable() {
        return false;
    }

    @Override
    public void setOnPreparedListener(OnPreparedListener listener) {
        if (null != mBackEndMediaPlayer) {
            if (null != listener) {
                final OnPreparedListener finalListener = listener;
                mBackEndMediaPlayer.setOnPreparedListener(new OnPreparedListener() {
                    @Override
                    public void onPrepared(IMediaPlayer mp) {
                        finalListener.onPrepared(MediaPlayerProxy.this);
                    }
                });

            } else {
                mBackEndMediaPlayer.setOnPreparedListener(null);
            }
        }
    }

    @Override
    public void setOnCompletionListener(OnCompletionListener listener) {
        if (null != mBackEndMediaPlayer) {
            if (null != listener) {
                final OnCompletionListener finalListener = listener;
                mBackEndMediaPlayer.setOnCompletionListener(new OnCompletionListener() {
                    @Override
                    public void onCompletion(IMediaPlayer mp) {
                        finalListener.onCompletion(MediaPlayerProxy.this);
                    }
                });

            } else {
                mBackEndMediaPlayer.setOnCompletionListener(null);
            }
        }
    }

    @Override
    public void setOnBufferingUpdateListener(OnBufferingUpdateListener listener) {
        if (null != mBackEndMediaPlayer) {
            if (null != listener) {
                final OnBufferingUpdateListener finalListener = listener;
                mBackEndMediaPlayer.setOnBufferingUpdateListener(new OnBufferingUpdateListener() {
                    @Override
                    public void onBufferingUpdate(IMediaPlayer mp, int percent) {
                        finalListener.onBufferingUpdate(MediaPlayerProxy.this, percent);
                    }
                });
            } else {
                mBackEndMediaPlayer.setOnBufferingUpdateListener(null);
            }
        }
    }

    @Override
    public void setOnSeekCompleteListener(OnSeekCompleteListener listener) {
        if (null != mBackEndMediaPlayer) {
            if (null != listener) {
                final OnSeekCompleteListener finalListener = listener;
                mBackEndMediaPlayer.setOnSeekCompleteListener(new OnSeekCompleteListener() {
                    @Override
                    public void onSeekComplete(IMediaPlayer mp) {
                        finalListener.onSeekComplete(MediaPlayerProxy.this);
                    }
                });
            } else {
                mBackEndMediaPlayer.setOnSeekCompleteListener(null);
            }
        }
    }

    @Override
    public void setOnVideoSizeChangedListener(OnVideoSizeChangedListener listener) {
        if (null != mBackEndMediaPlayer) {
            if (null != listener) {
                final OnVideoSizeChangedListener finalListener = listener;
                mBackEndMediaPlayer.setOnVideoSizeChangedListener(new OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(IMediaPlayer mp,
                                                   int width, int height,
                                                   int sar_num, int sar_den) {
                        finalListener.onVideoSizeChanged(MediaPlayerProxy.this,
                                width, height,
                                sar_num, sar_den);
                    }
                });

            } else {
                mBackEndMediaPlayer.setOnVideoSizeChangedListener(null);
            }
        }
    }

    @Override
    public void setOnErrorListener(OnErrorListener listener) {
        if (null != mBackEndMediaPlayer) {
            if (null != listener) {
                final OnErrorListener finalListener = listener;
                mBackEndMediaPlayer.setOnErrorListener(new OnErrorListener() {
                    @Override
                    public boolean onError(IMediaPlayer mp, int what, int extra) {
                        return finalListener.onError(MediaPlayerProxy.this, what, extra);
                    }
                });
            } else {
                mBackEndMediaPlayer.setOnErrorListener(null);
            }
        }
    }

    @Override
    public void setOnInfoListener(OnInfoListener listener) {
        if (null != mBackEndMediaPlayer) {
            if (null != listener) {
                final OnInfoListener finalListener = listener;
                mBackEndMediaPlayer.setOnInfoListener(new OnInfoListener() {
                    @Override
                    public boolean onInfo(IMediaPlayer mp, int what, int extra) {
                        return finalListener.onInfo(MediaPlayerProxy.this, what, extra);
                    }
                });
            } else {
                mBackEndMediaPlayer.setOnInfoListener(null);
            }
        }
    }

    @Override
    public void setOnTimedTextListener(OnTimedTextListener listener) {
        if (null != mBackEndMediaPlayer) {
            if (null != listener) {
                final OnTimedTextListener finalListener = listener;
                mBackEndMediaPlayer.setOnTimedTextListener(new OnTimedTextListener() {
                    @Override
                    public void onTimedText(IMediaPlayer mp, dTimedText text) {
                        finalListener.onTimedText(MediaPlayerProxy.this, text);
                    }
                });

            } else {
                mBackEndMediaPlayer.setOnTimedTextListener(null);
            }
        }
    }

    @Override
    public void setAudioStreamType(int streamType) {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.setAudioStreamType(streamType);
        }
    }

    @Deprecated
    @Override
    public void setKeepInBackground(boolean keepInBackground) {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.setKeepInBackground(keepInBackground);
        }
    }

    @Override
    public int getVideoSarNum() {
        return null != mBackEndMediaPlayer ? mBackEndMediaPlayer.getVideoSarNum() : 0;
    }

    @Override
    public int getVideoSarDen() {
        return null != mBackEndMediaPlayer ? mBackEndMediaPlayer.getVideoSarDen() : 0;
    }

    @Deprecated
    @Override
    public void setWakeMode(Context context, int mode) {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.setWakeMode(context, mode);
        }
    }

    @Override
    public void setLooping(boolean looping) {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.setLooping(looping);
        }
    }

    @Override
    public boolean isLooping() {
        return null != mBackEndMediaPlayer && mBackEndMediaPlayer.isLooping();
    }

    @Override
    public ITrackInfo[] getTrackInfo() {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.getTrackInfo();
        }
        return null;
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void setSurface(Surface surface) {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.setSurface(surface);
        }
    }

    @Override
    public void setDataSource(IMediaDataSource mediaDataSource) {
        if (null != mBackEndMediaPlayer) {
            mBackEndMediaPlayer.setDataSource(mediaDataSource);
        }
    }
}
