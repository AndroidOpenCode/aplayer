package dai.android.play;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Map;

import dai.android.play.log.Logger;
import dai.android.play.utility.DisplayHelper;
import dai.android.player.AndroidMediaPlayer;
import dai.android.player.IMediaPlayer;

public class VideoView extends FrameLayout implements IBasePlayer {

    private static final String TAG = VideoView.class.getSimpleName();

    //----------------------------------------------------------------------------------------------
    // the player status of current play
    //----------------------------------------------------------------------------------------------
    public static final int STATE_ERROR = -1;
    public static final int STATE_IDLE = 0;
    public static final int STATE_PREPARING = 1;
    public static final int STATE_PREPARED = 2;
    public static final int STATE_PLAYING = 3;
    public static final int STATE_PAUSED = 4;
    public static final int STATE_BUFFERING_PLAYING = 5;
    public static final int STATE_BUFFERING_PAUSED = 6;
    public static final int STATE_COMPLETED = 7;


    // show the video view
    public static final int VIEW_SURFACE = 0;
    public static final int VIEW_GLSURFACE = 1;
    public static final int VIEW_TEXTURE = 2;
    private int mViewType = VIEW_SURFACE;

    private FrameLayout mContainer;

    private SurfaceView m0SurfaceView;
    private GLSurfaceView m1GLSurfaceView;
    private TextureView m2TextureView;

    // screen status
    private static final int SCREEN_FULL = 1;
    private static final int SCREEN_NORMAL = 0;
    private int mScreenStatus = SCREEN_NORMAL;

    // the real player object
    private IMediaPlayer mMediaPlayer;
    private AudioManager mAudioManager;

    // the player ui controller
    private AbstractPlayerController mController;

    // the play url
    private String mUrl;
    private Map<String, String> mHeaders;

    // the current player state
    private int mCurrentPlayerState = STATE_IDLE;

    // the surface
    private SurfaceTexture mSurfaceTexture;
    private Surface mSurface;

    // skip to position play
    private long mSkipToPosition;

    // the buffer percentage
    private int mBufferPercentage;

    // a locker
    private final Object LOCK = new Object();

    public VideoView(Context context) {
        this(context, null);
    }

    public VideoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        mContainer = new FrameLayout(context);
        mContainer.setBackgroundColor(Color.BLACK);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        addView(mContainer, params);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.VideoView);
        if (null != a) {
            mViewType = a.getInt(R.styleable.VideoView_type, VIEW_SURFACE);
            a.recycle();
        }

        //initDisplayVideoView();

        // init the handler
        mWorkThread.start();
        mH = new myHandler(this, mWorkThread.getLooper());
    }

    public void setPlayer(IMediaPlayer player) {
        if (null == player) {
            mMediaPlayer = new AndroidMediaPlayer();
        } else {
            mMediaPlayer = player;
        }
    }

    public void setController(AbstractPlayerController controller) {
        if (null == controller) {
            if (null != mController) {
                mContainer.removeView(mContainer);
            }
            return;
        }

        mContainer.removeView(controller);
        mController = controller;
        mController.reset();
        mController.setVideoView(this);
        ViewGroup.LayoutParams params = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mContainer.addView(mController, params);
    }


    @Override
    public void setFullScreen(boolean fullscreen) {
        setFullScreen(fullscreen, true);
    }

    public void setFullScreen(boolean fullscreen, boolean oneDirection) {
        // this code read
        // https://www.jianshu.com/p/420f7b14d6f6

        if (fullscreen && mScreenStatus == SCREEN_FULL) return;
        if (!fullscreen && mScreenStatus == SCREEN_NORMAL) return;

        ViewGroup contentView = DisplayHelper.scanForActivity(getContext())
                .findViewById(android.R.id.content);
        ViewGroup.LayoutParams newParams = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );

        if (fullscreen) {
            mScreenStatus = SCREEN_FULL;

            // hide the actionbar
            DisplayHelper.hideActionBar(getContext());
            // landscape screen
            if (!oneDirection) {
                DisplayHelper.scanForActivity(getContext()).setRequestedOrientation(
                        ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                );
            }

            removeView(mContainer);
            contentView.addView(mContainer, newParams);
            if (null != mController) {
                mController.setFocusable(true);
                mController.setFocusableInTouchMode(true);
                mController.requestFocus();
            }

        } else {
            mScreenStatus = SCREEN_NORMAL;

            // show actionbar
            DisplayHelper.showActionBar(getContext());
            // portrait screen
            if (!oneDirection) {
                DisplayHelper.scanForActivity(getContext()).setRequestedOrientation(
                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                );
            }

            contentView.removeView(mContainer);
            addView(mContainer, newParams);
        }
    }

    @Override
    public void setUp(String url, Map<String, String> headers) {
        mUrl = url;
        mHeaders = headers;
    }

    @Override
    public void start() {
        Logger.i(TAG, "start: current state:" + mCurrentPlayerState);

        if (mCurrentPlayerState == STATE_IDLE) {
            initAudioManager();
            initMediaPlayer();
            registerDisplayView();
        } else {
            // some log or some other tell user only idle can start
            //mMediaPlayer.start();
        }
    }

    @Override
    public void start(long position) {
        Logger.i(TAG, "start at position: " + position);

        mSkipToPosition = position;
        start();
    }

    @Override
    public void restart() {
        Logger.i(TAG, "restart: current state:" + mCurrentPlayerState);

        switch (mCurrentPlayerState) {
            case STATE_PAUSED: {
                mMediaPlayer.start();
                mCurrentPlayerState = STATE_PLAYING;
                handleMessage(STATE_PLAYING);

                break;
            }

            case STATE_BUFFERING_PAUSED: {
                mMediaPlayer.start();
                mCurrentPlayerState = STATE_BUFFERING_PLAYING;
                handleMessage(STATE_BUFFERING_PLAYING);

                break;
            }

            case STATE_COMPLETED:
            case STATE_ERROR: {
                mMediaPlayer.reset();
                openMediaPlayer();

                break;
            }
        }
    }

    @Override
    public void pause() {
        if (mMediaPlayer == null) return;

        if (mCurrentPlayerState == STATE_PLAYING) {
            mMediaPlayer.pause();
            mCurrentPlayerState = STATE_PAUSED;
            handleMessage(STATE_PAUSED);
        }

        if (mCurrentPlayerState == STATE_BUFFERING_PLAYING) {
            mMediaPlayer.pause();
            mCurrentPlayerState = STATE_BUFFERING_PAUSED;
            handleMessage(STATE_BUFFERING_PAUSED);
        }
    }

    @Override
    public void seekTo(long pos) {
        if (mMediaPlayer == null) {
            return;
        }

        mMediaPlayer.seekTo(pos);
    }

    @Override
    public boolean isFullScreen() {
        return mScreenStatus == SCREEN_FULL;
    }

    @Override
    public boolean isNormalWindow() {
        return mScreenStatus == SCREEN_NORMAL;
    }


    @Override
    public void releasePlayer() {
        if (null != mAudioManager) {
            mAudioManager.abandonAudioFocus(null);
            mAudioManager = null;
        }

        if (null != mMediaPlayer) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    @Override
    public void release() {
        if (isFullScreen()) {
            setFullScreen(false);
        }

        //
        releasePlayer();

        if (null != mController) {
            mController.reset();
        }

        Runtime.getRuntime().gc();
    }

    @Override
    public long getDuration() {
        return null != mMediaPlayer ? mMediaPlayer.getDuration() : 0;
    }

    @Override
    public long getCurrentPosition() {
        return mMediaPlayer != null ? mMediaPlayer.getCurrentPosition() : 0;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public int getVolume() {
        if (null != mAudioManager) {
            return mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        }
        return 0;
    }

    @Override
    public int getMaxVolume() {
        if (null != mAudioManager) {
            return mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        }
        return 0;
    }

    @Override
    public boolean isIdle() {
        return mCurrentPlayerState == STATE_IDLE;
    }

    @Override
    public boolean isPreparing() {
        return mCurrentPlayerState == STATE_PREPARING;
    }

    @Override
    public boolean isPrepared() {
        return mCurrentPlayerState == STATE_PREPARED;
    }

    @Override
    public boolean isBufferingPlaying() {
        return mCurrentPlayerState == STATE_BUFFERING_PLAYING;
    }

    @Override
    public boolean isBufferingPaused() {
        return mCurrentPlayerState == STATE_BUFFERING_PAUSED;
    }

    @Override
    public boolean isPlaying() {
        return mCurrentPlayerState == STATE_PLAYING;
    }

    @Override
    public boolean isPaused() {
        return mCurrentPlayerState == STATE_PAUSED;
    }

    @Override
    public boolean isError() {
        return mCurrentPlayerState == STATE_ERROR;
    }

    @Override
    public boolean isCompleted() {
        return mCurrentPlayerState == STATE_COMPLETED;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK && action == KeyEvent.ACTION_UP) {
            //setScreen(oldWidth, oldHeight);
            setFullScreen(false);
            return false;
        }

        return true;

        //return super.dispatchKeyEvent(event);
    }


    /**
     * get the video real show type
     *
     * @return current view type
     * <p>
     * 0 - VIEW_SURFACE   : the real layer is Surface
     * 1 - VIEW_GLSURFACE :
     * </p>
     */
    public int getVideoLayerType() {
        return mViewType;
    }

    private void initDisplayVideoView() {

        switch (mViewType) {
            case VIEW_GLSURFACE: {
                initGLSurface();
                break;
            }

            case VIEW_TEXTURE: {
                makeOrAddTexture();
                break;
            }

            case VIEW_SURFACE:
            default: {
                ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                        LayoutParams.MATCH_PARENT,
                        LayoutParams.MATCH_PARENT
                );
                m0SurfaceView = new SurfaceView(getContext());
                mContainer.addView(m0SurfaceView, params);
                break;
            }
        }
    }

    private void initGLSurface() {
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT
        );
        m1GLSurfaceView = new GLSurfaceView(getContext());
        m1GLSurfaceView.setLayoutParams(params);
        mContainer.addView(m1GLSurfaceView, params);
    }

    private void makeOrAddTexture() {
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT
        );
        if (null == m2TextureView) {
            m2TextureView = new TextureView(getContext());
            m2TextureView.setSurfaceTextureListener(mTextureSurfaceListener);
        } else {
            mContainer.removeView(m2TextureView);
        }
        mContainer.addView(m2TextureView, 0, params);
    }

    private void registerDisplayView() {
        switch (mViewType) {
            case VIEW_TEXTURE: {
                makeOrAddTexture();
                break;
            }
        }
    }

    private void initAudioManager() {
        if (null == mAudioManager) {
            mAudioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
            if (null != mAudioManager) {
                mAudioManager.requestAudioFocus(
                        null,
                        AudioManager.STREAM_MUSIC,
                        AudioManager.AUDIOFOCUS_GAIN
                );
            }
        }
    }

    private void initMediaPlayer() {
        if (null == mMediaPlayer) {
            mMediaPlayer = new AndroidMediaPlayer();
        }

        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }


    // deal TextureView
    private TextureView.SurfaceTextureListener mTextureSurfaceListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            if (null == mSurfaceTexture) {
                mSurfaceTexture = surface;
                openMediaPlayer();
            } else {
                m2TextureView.setSurfaceTexture(mSurfaceTexture);
            }
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return mSurfaceTexture == null;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    };

    private void openMediaPlayer() {
        mContainer.setKeepScreenOn(true);
        mMediaPlayer.setOnPreparedListener(mOnPreparedListener);
        mMediaPlayer.setOnVideoSizeChangedListener(mOnVideoSizeChangedListener);
        mMediaPlayer.setOnCompletionListener(mOnCompletionListener);
        mMediaPlayer.setOnErrorListener(mOnErrorListener);
        mMediaPlayer.setOnInfoListener(mOnInfoListener);
        mMediaPlayer.setOnBufferingUpdateListener(mOnBufferingUpdateListener);

        // set data source
        try {
            mMediaPlayer.setDataSource(
                    getContext().getApplicationContext(),
                    Uri.parse(mUrl),
                    mHeaders
            );

            if (mSurface == null) {
                mSurface = new Surface(mSurfaceTexture);
            }
            mMediaPlayer.setSurface(mSurface);
            mMediaPlayer.prepareAsync();
            mCurrentPlayerState = STATE_PREPARING;

        } catch (IOException e) {
        }
    }

    //----------------------------------------------------------------------------------------------
    // MediaPlayer Listeners
    //----------------------------------------------------------------------------------------------
    private IMediaPlayer.OnPreparedListener mOnPreparedListener =
            new IMediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(final IMediaPlayer mp) {
                    Logger.i(TAG, "OnPreparedListener");

                    mCurrentPlayerState = STATE_PREPARED;
                    if (null != mH) {
                        mH.post(new Runnable() {
                            @Override
                            public void run() {
                                synchronized (LOCK) {
                                    if (null != mController) {
                                        mController.onPlayStateChanged(STATE_PREPARED);
                                        /////mController.onPrepared(mp);
                                    }
                                    mp.start();
                                    if (0 != mSkipToPosition) {
                                        mp.seekTo(mSkipToPosition);
                                    }
                                }
                            }
                        });
                    }
                }
            };

    private IMediaPlayer.OnVideoSizeChangedListener mOnVideoSizeChangedListener =
            new IMediaPlayer.OnVideoSizeChangedListener() {
                @Override
                public void onVideoSizeChanged(IMediaPlayer mp,
                                               int width, int height,
                                               int sar_num, int sar_den) {
                    Logger.i(TAG, "OnVideoSizeChangedListener");

                    switch (mViewType) {
                        case VIEW_TEXTURE: {
                            //if (null != m2TextureView) {
                            //}
                            break;
                        }
                    }

                }
            };

    private IMediaPlayer.OnCompletionListener mOnCompletionListener =
            new IMediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(final IMediaPlayer mp) {
                    Logger.i(TAG, "OnCompletionListener");

                    mCurrentPlayerState = STATE_COMPLETED;
                    if (null != mH) {
                        mH.post(new Runnable() {
                            @Override
                            public void run() {
                                synchronized (LOCK) {
                                    if (null != mController) {
                                        mController.onPlayStateChanged(STATE_COMPLETED);
                                        /////mController.onCompletion(mp);
                                    }
                                }
                            }
                        });
                    }

                    if (null != mContainer) {
                        mContainer.setKeepScreenOn(false);
                    }

                    //if (isFullScreen()) {
                    //    setFullScreen(false);
                    //}
                }
            };

    private IMediaPlayer.OnErrorListener mOnErrorListener =
            new IMediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(final IMediaPlayer mp, final int what, final int extra) {
                    Logger.i(TAG, "onError");

                    mCurrentPlayerState = STATE_ERROR;
                    if (null != mH) {
                        mH.post(new Runnable() {
                            @Override
                            public void run() {
                                synchronized (LOCK) {
                                    if (null != mController) {
                                        ////mController.onError(mp, what, extra);
                                        mController.onPlayStateChanged(STATE_ERROR);
                                    }
                                }
                            }
                        });
                    }
                    return true;
                }
            };

    private IMediaPlayer.OnInfoListener mOnInfoListener =
            new IMediaPlayer.OnInfoListener() {
                @Override
                public boolean onInfo(final IMediaPlayer mp, final int what, final int extra) {
                    Logger.i(TAG, "OnInfoListener: what=" + what + ", extra=" + extra);

                    switch (what) {
                        case IMediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START: {
                            mCurrentPlayerState = STATE_PLAYING;
                            handleMessage(STATE_PLAYING);
                            break;
                        }

                        case IMediaPlayer.MEDIA_INFO_BUFFERING_START: {
                            if (mCurrentPlayerState == STATE_PAUSED ||
                                    mCurrentPlayerState == STATE_BUFFERING_PAUSED) {
                                mCurrentPlayerState = STATE_BUFFERING_PAUSED;
                                handleMessage(STATE_BUFFERING_PAUSED);
                            } else {
                                mCurrentPlayerState = STATE_BUFFERING_PLAYING;
                                handleMessage(STATE_BUFFERING_PLAYING);
                            }
                            break;
                        }

                        case IMediaPlayer.MEDIA_INFO_BUFFERING_END: {
                            if (mCurrentPlayerState == STATE_BUFFERING_PLAYING) {
                                mCurrentPlayerState = STATE_PLAYING;
                                handleMessage(STATE_PLAYING);
                            }
                            if (mCurrentPlayerState == STATE_BUFFERING_PAUSED) {
                                mCurrentPlayerState = STATE_PAUSED;
                                handleMessage(STATE_PAUSED);
                            }
                            break;
                        }

                        case IMediaPlayer.MEDIA_INFO_VIDEO_ROTATION_CHANGED: {
                            // on tv no this operation
                            break;
                        }

                        default: {
                            // other case
                            break;
                        }
                    }

                    return true;
                }
            };

    private IMediaPlayer.OnBufferingUpdateListener mOnBufferingUpdateListener =
            new IMediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(final IMediaPlayer mp, final int percent) {
                    //  Logger.i(TAG, "OnBufferingUpdateListener: percent=" + percent);

                    mBufferPercentage = percent;

                    // the implement function must start at a new thread
                    if (null != mController) {
                        mController.onBufferingUpdate(mp, percent);
                    }
                }
            };


    private void handleMessage(final int status) {
        Logger.i(TAG, "handleMessage: status=" + status);

        if (null != mH && null != mController) {
            mH.post(new Runnable() {
                @Override
                public void run() {
                    mController.onPlayStateChanged(status);
                }
            });
        }
    }

    // real exec body of handler
    private void handleMessage(Message msg) {
    }


    //----------------------------------------------------------------------------------------------
    // class Handler and
    //----------------------------------------------------------------------------------------------
    private HandlerThread mWorkThread = new HandlerThread("VideoViewThread");
    private myHandler mH;

    private static class myHandler extends Handler {
        private final WeakReference<VideoView> mRefVideoView;

        myHandler(VideoView object, Looper looper) {
            super(looper);
            mRefVideoView = new WeakReference<>(object);
        }

        @Override
        public void handleMessage(Message msg) {
            if (null != mRefVideoView && null != mRefVideoView.get()) {
                mRefVideoView.get().handleMessage(msg);
            }
        }
    }


}
