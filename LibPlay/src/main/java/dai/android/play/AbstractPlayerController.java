package dai.android.play;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.FrameLayout;

import dai.android.player.IMediaPlayer;

public abstract class AbstractPlayerController extends FrameLayout {

    protected VideoView mVideoView;

    public AbstractPlayerController(@NonNull Context context) {
        super(context, null);
    }

    protected abstract void reset();

    public void setVideoView(VideoView videoView) {
        mVideoView = videoView;
    }

    protected abstract void onPlayStateChanged(int playState);

    /**
     * function of IMediaPlayer.OnPreparedListener
     *
     * @param mp
     */
    //protected abstract void onPrepared(final IMediaPlayer mp);

    /**
     * function of IMediaPlayer.OnVideoSizeChangedListener
     *
     * @param mp
     * @param width
     * @param height
     * @param sar_num
     * @param sar_den
     */
    //protected abstract void onVideoSizeChanged(final IMediaPlayer mp,
    //                                           final int width, final int height,
    //                                           final int sar_num, final int sar_den);

    /**
     * function of IMediaPlayer.OnCompletionListener
     *
     * @param mp
     */
    //protected abstract void onCompletion(IMediaPlayer mp);

    /**
     * function of IMediaPlayer.OnErrorListener
     *
     * @param mp
     * @param what
     * @param extra
     * @return
     */
    //protected abstract boolean onError(IMediaPlayer mp, int what, int extra);

    /**
     * function of IMediaPlayer.OnInfoListener
     *
     * @param mp
     * @param what
     * @param extra
     * @return
     */
    //protected abstract boolean onInfo(IMediaPlayer mp, int what, int extra);


    /**
     * IMediaPlayer.OnBufferingUpdateListener
     *
     * @param mp
     * @param percent
     */
    protected abstract void onBufferingUpdate(IMediaPlayer mp, int percent);


}
