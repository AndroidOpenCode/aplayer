package dai.android.play.log;

import android.util.Log;

public final class Logger {

    private static final String TAG = "libPlay";

    public static void v(String tag, String message) {
        v(tag, message, null);
    }

    public static void v(String tag, String message, Throwable throwable) {
        printlog(VERBOSE, tag, message, throwable);
    }

    public static void d(String tag, String message) {
        d(tag, message, null);
    }

    public static void d(String tag, String message, Throwable throwable) {
        printlog(DEBUG, tag, message, throwable);
    }

    public static void i(String tag, String message) {
        i(tag, message, null);
    }

    public static void i(String tag, String message, Throwable throwable) {
        printlog(INFO, tag, message, throwable);
    }

    public void w(String tag, String message) {
        w(tag, message, null);
    }

    public static void w(String tag, String message, Throwable throwable) {
        printlog(WARN, tag, message, throwable);
    }


    public static void e(String tag, String message) {
        e(tag, message, null);
    }

    public static void e(String tag, String message, Throwable throwable) {
        printlog(ERROR, tag, message, throwable);
    }


    private static final int VERBOSE = Log.VERBOSE;
    private static final int DEBUG = Log.DEBUG;
    private static final int INFO = Log.INFO;
    private static final int WARN = Log.WARN;
    private static final int ERROR = Log.ERROR;
    private static final int ASSERT = Log.ASSERT;

    private static final void printlog(int level, String tag, String message, Throwable throwable) {

        String strMessage = "[ " + tag + " ]:" + message;
        switch (level) {
            case VERBOSE: {
                Log.v(TAG, strMessage, throwable);
                break;
            }

            case DEBUG: {
                Log.d(TAG, strMessage, throwable);
                break;
            }

            case INFO: {
                Log.i(TAG, strMessage, throwable);
                break;
            }

            case WARN: {
                Log.w(TAG, strMessage, throwable);
                break;
            }

            case ERROR: {
                Log.e(TAG, strMessage, throwable);
                break;
            }

        }
    }
}
