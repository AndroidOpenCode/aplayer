package dai.android.play;

import java.util.Map;

public interface IBasePlayer {

    void setUp(String url, Map<String, String> headers);

    void start();

    void start(long position);

    void restart();

    void pause();

    void seekTo(long pos);


    boolean isFullScreen();

    boolean isNormalWindow();


    /**
     * set the player video show at fullscreen or normal
     *
     * @param fullscreen
     */
    void setFullScreen(boolean fullscreen);

    void releasePlayer();

    void release();

    long getDuration();

    long getCurrentPosition();

    int getBufferPercentage();

    int getVolume();

    int getMaxVolume();


    // the player function
    boolean isIdle();

    boolean isPreparing();

    boolean isPrepared();

    boolean isBufferingPlaying();

    boolean isBufferingPaused();

    boolean isPlaying();

    boolean isPaused();

    boolean isError();

    boolean isCompleted();
}
