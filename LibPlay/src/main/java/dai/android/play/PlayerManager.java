package dai.android.play;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.DisplayMetrics;

import java.lang.ref.WeakReference;

import dai.android.play.utility.DisplayHelper;
import dai.android.player.AndroidMediaPlayer;
import dai.android.player.IMediaPlayer;
import dai.android.player.dTimedText;

public final class PlayerManager {

    private static final String TAG = PlayerManager.class.getSimpleName();

    //---------------------------------------------------------------------
    // private field
    //---------------------------------------------------------------------
    private WeakReference<VideoView> mRefVideoView;

    private HandlerThread mWorkThread = new HandlerThread(TAG);
    private myHandler H;

    private IMediaPlayer mMediaPlayer;

    // current video view width and height;
    private int mVideoWidth, mVideoHeight;

    // full screen width and height
    private int mFullScreenWidth, mFullScreenHeight;

    public PlayerManager(VideoView videoView) {
        this(null, videoView);
    }

    public PlayerManager(IMediaPlayer mediaPlayer, VideoView videoView) {
        this(mediaPlayer, videoView, 0, 0);
    }

    public PlayerManager(IMediaPlayer mediaPlayer,
                         VideoView videoView,
                         int fullWidth, int fullHeight) {
        mWorkThread.start();
        H = new myHandler(this, mWorkThread.getLooper());

        if (null == mediaPlayer) {
            mMediaPlayer = new AndroidMediaPlayer();
        } else {
            mMediaPlayer = mediaPlayer;
        }

        mRefVideoView = new WeakReference<>(videoView);

        setFullScreenInfo(fullWidth, fullHeight);
    }

    public void setFullScreenInfo(int width, int height) {
        if (width <= 0 || height <= 0) {
            if (null != mRefVideoView && null != mRefVideoView.get()) {
                DisplayMetrics display = DisplayHelper.getDisplayMetrics(mRefVideoView.get().getContext());
                mFullScreenWidth = DisplayHelper.getScreenWidth(display);
                mFullScreenHeight = DisplayHelper.getScreenHeight(display);
            }
        } else {
            mFullScreenWidth = width;
            mFullScreenHeight = height;
        }
    }

    //---------------------------------------------------------------------
    // set listener
    //---------------------------------------------------------------------
    public void setOnPreparedListener(final IMediaPlayer.OnPreparedListener listener) {
        if (null != listener && null != mMediaPlayer) {
            mMediaPlayer.setOnPreparedListener(
                    new IMediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(final IMediaPlayer mp) {
                            H.post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onPrepared(mp);
                                }
                            });
                        }
                    }
            );
        }
    }

    public void setOnCompletionListener(final IMediaPlayer.OnCompletionListener listener) {
        if (null != listener && null != mMediaPlayer) {
            mMediaPlayer.setOnCompletionListener(
                    new IMediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(final IMediaPlayer mp) {
                            H.post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onCompletion(mp);
                                }
                            });
                        }
                    }
            );
        }
    }

    public void setOnBufferingUpdateListener(final IMediaPlayer.OnBufferingUpdateListener listener) {
        if (null != listener && null != mMediaPlayer) {
            mMediaPlayer.setOnBufferingUpdateListener(
                    new IMediaPlayer.OnBufferingUpdateListener() {
                        @Override
                        public void onBufferingUpdate(final IMediaPlayer mp, final int percent) {
                            H.post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onBufferingUpdate(mp, percent);
                                }
                            });
                        }
                    }
            );
        }
    }

    public void setOnSeekCompleteListener(final IMediaPlayer.OnSeekCompleteListener listener) {
        if (null != listener && null != mMediaPlayer) {
            mMediaPlayer.setOnSeekCompleteListener(new IMediaPlayer.OnSeekCompleteListener() {
                @Override
                public void onSeekComplete(final IMediaPlayer mp) {
                    H.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onSeekComplete(mp);
                        }
                    });
                }
            });
        }
    }

    public void setOnVideoSizeChangedListener(final IMediaPlayer.OnVideoSizeChangedListener listener) {
        if (null != listener && null != mMediaPlayer) {
            mMediaPlayer.setOnVideoSizeChangedListener(new IMediaPlayer.OnVideoSizeChangedListener() {
                @Override
                public void onVideoSizeChanged(final IMediaPlayer mp,
                                               final int width, final int height,
                                               final int sar_num, final int sar_den) {
                    H.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onVideoSizeChanged(mp, width, height, sar_num, sar_den);
                        }
                    });
                }
            });
        }
    }

    public void setOnErrorListener(final IMediaPlayer.OnErrorListener listener) {
        setOnErrorListener(listener, false);
    }

    public void setOnErrorListener(final IMediaPlayer.OnErrorListener listener, final boolean runAtThread) {
        if (null != listener && null != mMediaPlayer) {
            mMediaPlayer.setOnErrorListener(new IMediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(final IMediaPlayer mp, final int what, final int extra) {
                    boolean result = true;
                    if (runAtThread) {
                        H.post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onError(mp, what, extra);
                            }
                        });
                    } else {
                        result = listener.onError(mp, what, extra);
                    }
                    return result;
                }
            });
        }
    }

    public void setOnInfoListener(final IMediaPlayer.OnInfoListener listener) {
        setOnInfoListener(listener, false);
    }

    public void setOnInfoListener(final IMediaPlayer.OnInfoListener listener, final boolean runAtThread) {
        if (null != listener && null != mMediaPlayer) {
            mMediaPlayer.setOnInfoListener(new IMediaPlayer.OnInfoListener() {
                @Override
                public boolean onInfo(final IMediaPlayer mp, final int what, final int extra) {
                    boolean result = true;
                    if (runAtThread) {
                        H.post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onInfo(mp, what, extra);
                            }
                        });
                    } else {
                        result = listener.onInfo(mp, what, extra);
                    }
                    return result;
                }
            });
        }
    }

    public void setOnTimedTextListener(final IMediaPlayer.OnTimedTextListener listener) {
        if (null != listener && null != mMediaPlayer) {
            mMediaPlayer.setOnTimedTextListener(new IMediaPlayer.OnTimedTextListener() {
                @Override
                public void onTimedText(final IMediaPlayer mp, final dTimedText text) {
                    H.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onTimedText(mp, text);
                        }
                    });
                }
            });
        }
    }


    public void setFullScreen(boolean full) {
        if (null == mRefVideoView || null == mRefVideoView.get())
            return;

        mRefVideoView.get().setFullScreen(full);
    }

    public boolean isFullScreen() {
        if (null == mRefVideoView || null == mRefVideoView.get())
            return false;

        return mRefVideoView.get().isFullScreen();
    }


    private static class myHandler extends Handler {
        private WeakReference<PlayerManager> mRefManager;

        myHandler(PlayerManager manager, Looper looper) {
            super(looper);
            mRefManager = new WeakReference<>(manager);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (null != mRefManager && null != mRefManager.get()) {
                mRefManager.get().handleMessage(msg);
            }
        }
    }

    private void handleMessage(Message msg) {
    }

}
