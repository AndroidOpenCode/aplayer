package dai.android.play.utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.DisplayMetrics;
import android.view.WindowManager;

public final class DisplayHelper {

    public static DisplayMetrics getDisplayMetrics(Context context) {
        Resources resources = context.getResources();
        return resources.getDisplayMetrics();
    }

    public static int getScreenWidth(DisplayMetrics display) {
        return display.widthPixels;
    }

    public static int getScreenHeight(DisplayMetrics display) {
        return display.heightPixels;
    }

    public static float getScreenDensity(DisplayMetrics display) {
        return display.density;
    }

    public static Activity scanForActivity(Context context) {
        if (null == context) return null;
        if (context instanceof Activity) {
            return (Activity) context;
        } else if (context instanceof ContextWrapper) {
            scanForActivity(((ContextWrapper) context).getBaseContext());
        }

        return null;
    }

    @SuppressLint("RestrictedApi")
    public static void showActionBar(Context context) {
        AppCompatActivity activity = getAppCompActivity(context);
        if (null == activity) return;

        ActionBar ab = activity.getSupportActionBar();
        if (null == ab) return;

        ab.setShowHideAnimationEnabled(false);
        ab.show();

        scanForActivity(context).getWindow().clearFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
    }

    @SuppressLint("RestrictedApi")
    public static void hideActionBar(Context context) {
        AppCompatActivity activity = getAppCompActivity(context);
        if (null == activity) return;

        ActionBar ab = activity.getSupportActionBar();
        if (null == ab) return;

        ab.setShowHideAnimationEnabled(false);
        ab.hide();

        scanForActivity(context).getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
    }


    private static AppCompatActivity getAppCompActivity(Context context) {
        if (context == null) return null;
        if (context instanceof AppCompatActivity) {
            return (AppCompatActivity) context;
        } else if (context instanceof ContextThemeWrapper) {
            return getAppCompActivity(((ContextThemeWrapper) context).getBaseContext());
        }
        return null;
    }
}
